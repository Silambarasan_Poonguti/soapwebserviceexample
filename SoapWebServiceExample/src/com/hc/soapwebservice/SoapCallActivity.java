package com.hc.soapwebservice;

import com.hirecraft.soapwebservice.R;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SoapCallActivity extends Activity {

	/* My second commit */
	
	TextView webserviceResponse;
	EditText getValue;
	Button celsiusButton, fahrenheitButton;
	private String TAG = "SoapCallActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_soap_call);

		celsiusButton = (Button) findViewById(R.id.btnCelsius);
		webserviceResponse = (TextView) findViewById(R.id.result);
		getValue = (EditText) findViewById(R.id.inputValue);

		celsiusButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				webserviceResponse.setText("");
				new SoapCelsiusCall().execute();

			}
		});

		fahrenheitButton = (Button) findViewById(R.id.btnFahrenheit);

		fahrenheitButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				webserviceResponse.setText("");
				new SoapFahrenheitCall().execute();

			}
		});
	}

	private class SoapCelsiusCall extends AsyncTask<String, Void, String> {

		String aResponse;
		String celsius;

		@Override
		protected String doInBackground(String... arg0) {
			try {

				// Create Webservice class object
				WebserviceCall com = new WebserviceCall();

				// Initialize variables
				celsius = getValue.getText().toString();

				// Call Webservice class method and pass values and get response
				aResponse = com.getCelsiusToFahrenheit("CelsiusToFahrenheit",
						celsius);
			} catch (Exception bug) {
				bug.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// Alert message to show web service response
			Toast.makeText(getApplicationContext(),
					celsius + " Celsius= " + aResponse, Toast.LENGTH_LONG)
					.show();

			Log.i(TAG, aResponse);

			webserviceResponse.setText("Celsius To Fahrenheit: " + aResponse);
			super.onPostExecute(result);
		}

	}

	private class SoapFahrenheitCall extends AsyncTask<String, Void, String> {

		String fahrenheit;
		String aResponse;

		@Override
		protected String doInBackground(String... arg0) {
			try {

				// Create Web service class object
				WebserviceCall com = new WebserviceCall();

				// Initialize variables
				fahrenheit = getValue.getText().toString();
				// Call Web service class method and pass values and get response
				aResponse = com.getfahrenheitToCelsius("FahrenheitToCelsius",
						fahrenheit);
			} catch (Exception bug) {
				bug.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// Alert message to show web service response
			Toast.makeText(getApplicationContext(),
					fahrenheit + " Fahrenheit= " + aResponse, Toast.LENGTH_LONG)
					.show();

			Log.i(TAG, aResponse);

			webserviceResponse.setText("Fahrenheit To Celsius: " + aResponse);
			super.onPostExecute(result);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.soap_call, menu);
		return true;
	}

}
