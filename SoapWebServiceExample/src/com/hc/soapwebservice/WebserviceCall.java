package com.hc.soapwebservice;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class WebserviceCall {

	/**
	 * Variable Declaration
	 * 
	 */
	private String namespace = "http://www.w3schools.com/webservices/";
	private String url = "http://www.w3schools.com/webservices/tempconvert.asmx";

	String SOAP_ACTION;
	SoapObject request = null, objMessages = null;
	SoapSerializationEnvelope envelope;
	HttpTransportSE androidHttpTransport;

	WebserviceCall() {
	}

	/**
	 * Set Envelope
	 */
	protected void SetEnvelope() {

		try {

			// Creating SOAP envelope
			envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

			// You can comment that line if your web service is not .NET one.
			envelope.dotNet = true;

			envelope.setOutputSoapObject(request);
			androidHttpTransport = new HttpTransportSE(url);
			androidHttpTransport.debug = true;

		} catch (Exception e) {
			System.out.println("Soap Exception" + e.toString());
		}
	}

	// MethodName variable is define for which webservice function will call
	public String getCelsiusToFahrenheit(String MethodName, String celsius) {

		try {
			SOAP_ACTION = namespace + MethodName;

			// Adding values to request object
			request = new SoapObject(namespace, MethodName);
			request.addProperty("Celsius", "" + celsius);

			SetEnvelope();

			try {

				// SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				// Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}

	public String getfahrenheitToCelsius(String MethodName, String fahrenheit) {

		try {
			SOAP_ACTION = namespace + MethodName;

			// Adding values to request object
			request = new SoapObject(namespace, MethodName);
			request.addProperty("Fahrenheit", "" + fahrenheit);

			SetEnvelope();

			try {

				// SOAP calling webservice
				androidHttpTransport.call(SOAP_ACTION, envelope);

				// Got Webservice response
				String result = envelope.getResponse().toString();

				return result;

			} catch (Exception e) {
				// TODO: handle exception
				return e.toString();
			}
		} catch (Exception e) {
			// TODO: handle exception
			return e.toString();
		}

	}

}
